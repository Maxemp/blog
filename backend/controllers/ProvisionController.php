<?php

namespace backend\controllers;

use blog\forms\manage\competition\ProvisionCreateForm;
use blog\forms\manage\competition\ProvisionEditForm;
use blog\manage\competition\ProvisionManageService;
use Yii;
use blog\entities\competition\Provision;
use backend\forms\ProvisionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProvisionController implements the CRUD actions for Provision model.
 */
class ProvisionController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], ProvisionManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Provision models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProvisionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Provision model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionDownload($id)
	{
		$model = $this->findModel($id);
		$file = $model->getUploadedFilePath('file');

		return Yii::$app->response->sendFile($file);
	}

    /**
     * Creates a new Provision model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ProvisionCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$provision = $this->service->create($form);
        		Yii::$app->session->setFlash('success', 'Положение успешно создано');
		        return $this->redirect(['view', 'id' => $provision->id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Provision model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $provision = $this->findModel($id);
        $form = new ProvisionEditForm($provision);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try{
            	$this->service->edit($form, $id);
            	Yii::$app->session->setFlash('success', 'Положение успешно изменено');
	            return $this->redirect(['view', 'id' => $id]);
            }catch (\RuntimeException $e){
            	Yii::$app->errorHandler->logException($e);
            	Yii::$app->session->setFlash('error', $e->getMessage());
            }

        }

        return $this->render('update', [
            'model' => $form,
	        'provision' => $provision
        ]);
    }

    /**
     * Deletes an existing Provision model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Provision model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Provision the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Provision::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
