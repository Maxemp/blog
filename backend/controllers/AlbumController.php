<?php

namespace backend\controllers;

use blog\forms\manage\album\AlbumEditForm;
use blog\helpers\AlbumHelper;
use yii\web\NotFoundHttpException;
use backend\forms\AlbumSearchForm;
use blog\entities\Album;
use blog\manage\AlbumManageService;
use yii\web\Controller;
use blog\forms\manage\album\AlbumCreateForm;
use \Yii;
use yii\filters\VerbFilter;

class AlbumController extends Controller
{

	private $service;

	public function __construct( $id, $module, array $config = [], AlbumManageService $service)
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model not found
	 */
	public function actionUpdate($id)
	{
		$album = $this->findModel($id);

		$form = new AlbumEditForm($album);
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->edit($album->id, $form);
				Yii::$app->session->setFlash('success', 'Album updated');
				return $this->redirect(['view', 'id' => $id]);
			}catch (\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render('update',
			[
				'model' => $form,
				'album' => $album
			]);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);
		return $this->render('view', [
			'model' => $model,
		]);
	}

	/**
	 * @return mixed
	 */
	public function actionCreate()
	{
		$form = new AlbumCreateForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$album = $this->service->create($form);
				Yii::$app->session->setFlash('success', 'Album created');
				return $this->redirect(['view', 'id' => $album->id]);
			}catch (\RuntimeException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render('create', ['model' => $form]);
	}

	/**
	 * 1234
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new AlbumSearchForm();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$photos = AlbumHelper::getPhotos($id);
		foreach ($photos as $photo){
			$photo->delete();
		}
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * @param $id
	 * @return Album the loaded model
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = Album::findOne($id)) !== null){
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}