<?php

namespace backend\controllers;

use blog\forms\manage\document\DocumentCreateForm;
use blog\forms\manage\document\DocumentEditForm;
use blog\manage\DocumentManageService;
use Yii;
use blog\entities\Document;
use backend\forms\DocumentSearchForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], DocumentManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
    public function actionDownload($id)
    {
    	$model = $this->findModel($id);
    	$file = $model->getUploadedFilePath('document');

    	return Yii::$app->response->sendFile($file);
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new DocumentCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
				$document = $this->service->create($form);
				Yii::$app->session->setFlash('success', 'Document uploaded.');
				return $this->redirect(['view', 'id' => $document->id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $document = $this->findModel($id);
	    $form = new DocumentEditForm($document);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$this->service->edit($id, $form);
        		Yii::$app->session->setFlash('success', 'Document updated');
        		return $this->redirect(['view', 'id' => $id]);
	        }catch(NotFoundHttpException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
