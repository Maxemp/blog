<?php

namespace backend\controllers;

use blog\forms\manage\Video\VideoForm;
use blog\manage\VideoManageService;
use Yii;
use blog\entities\Video;
use backend\forms\VideoSearchForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends Controller
{
	/**
	 * @var VideoManageService
	 */
	private $service;

	public function __construct( $id, $module, array $config = [], VideoManageService $service ) {
		parent::__construct( $id, $module, $config );
		$this->service = $service;
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Video models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideoSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Video model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Video model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new VideoForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()){
        	try{
        		$video = $this->service->create($form);
        		Yii::$app->session->setFlash('success', 'Video parsed.');
        		return $this->redirect(['view', 'id' => $video->id]);
	        }catch(\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing Video model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Video model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Video the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
