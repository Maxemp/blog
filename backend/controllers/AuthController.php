<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 15.02.2018
 * Time: 14:20
 */

namespace backend\controllers;


use blog\forms\auth\LoginForm;
use blog\services\auth\AuthService;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class AuthController  extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], AuthService $service ) {
		parent::__construct( $id, $module, $config );
		$this->service = $service;
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actionLogin()
	{
		if(!Yii::$app->user->isGuest){
			return $this->goHome();
		}

		$this->layout = 'main-login';

		$form = new LoginForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$user = $this->service->auth($form);
				Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 *30 : 0);
				return $this->goBack();
			}catch(\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}

		}

		return $this->render('login', ['model' => $form]);
	}

	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

}