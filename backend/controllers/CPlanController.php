<?php

namespace backend\controllers;

use blog\forms\manage\competition\CplanCreateForm;
use blog\forms\manage\competition\CplanEditForm;
use blog\manage\competition\CPlanManageService;
use Yii;
use blog\entities\competition\CPlan;
use backend\forms\CPlanSearchForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CPlanController implements the CRUD actions for CPlan model.
 */
class CplanController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], CPlanManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CPlan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CPlanSearchForm();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CPlan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CPlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new CplanCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$c_plan = $this->service->create($form);
        		Yii::$app->session->setFlash('success', 'Календарный план создан');
        		return $this->redirect(['view', 'id' => $c_plan->id]);
	        }catch(\RuntimeException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
	        }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing CPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $cplan = $this->findModel($id);
        $form = new CplanEditForm($cplan);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$this->service->edit($id, $form);
        		Yii::$app->session->setFlash('success', 'Календарный план изменен');
        		return $this->redirect(['view', 'id' => $id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }
            return $this->redirect(['view', 'id' => $form->id]);
        }

        return $this->render('update', [
            'model' => $form,
	        'cplan' => $cplan
        ]);
    }

    /**
     * Deletes an existing CPlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CPlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CPlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CPlan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
