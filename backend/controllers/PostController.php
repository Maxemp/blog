<?php

namespace backend\controllers;

use blog\forms\manage\Post\PostCreateForm;
use blog\forms\manage\Post\PostEditForm;
use blog\manage\PostManageService;
use Yii;
use blog\entities\Post;
use backend\forms\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], PostManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new PostCreateForm();
		$userId = Yii::$app->user->getId();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$post = $this->service->create($form, $userId);
        		Yii::$app->session->setFlash('success', 'Статься успешно создана');
		        return $this->redirect(['view', 'id' => $post->id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }

        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $post = $this->findModel($id);
        $form = new PostEditForm($post);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try {
        		$this->service->edit($form, $id);
        		Yii::$app->session->setFlash('success', 'Статья успешно изменена');
		        return $this->redirect(['view', 'id' => $id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }

        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
