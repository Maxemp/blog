<?php

namespace backend\controllers;

use blog\forms\manage\competition\ResultCreateForm;
use blog\forms\manage\competition\ResultEditForm;
use blog\manage\competition\ResultManageService;
use Yii;
use blog\entities\competition\Result;
use backend\forms\ResultSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ResultController implements the CRUD actions for Result model.
 */
class ResultController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], ResultManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actionDownload($id)
	{
		$model = $this->findModel($id);
		$file = $model->getUploadedFilePath('file');

		return Yii::$app->response->sendFile($file);
	}

    /**
     * Lists all Result models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResultSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Result model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Result model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ResultCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$result = $this->service->create($form);
        		Yii::$app->session->setFlash('success', 'Результат успешно создан.');
		        return $this->redirect(['view', 'id' => $result->id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }

        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Result model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $result = $this->findModel($id);
        $form = new ResultEditForm($result);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        	try{
        		$this->service->edit($form, $id);
        		Yii::$app->session->setFlash('success', 'Результат усепшно изменен');
		        return $this->redirect(['view', 'id' => $id]);
	        }catch (\RuntimeException $e){
        		Yii::$app->errorHandler->logException($e);
        		Yii::$app->session->setFlash('error', $e->getMessage());
	        }

        }

        return $this->render('update', [
            'model' => $form,
	        'result' => $result
        ]);
    }

    /**
     * Deletes an existing Result model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Result model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Result the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Result::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
