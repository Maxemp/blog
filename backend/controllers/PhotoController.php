<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 08.03.2018
 * Time: 11:15
 */

namespace backend\controllers;

use backend\forms\PhotoSearchForm;
use blog\forms\manage\Photo\PhotoEditForm;
use blog\forms\manage\Photo\PhotoCreateForm;
use yii\filters\VerbFilter;
use yii\web\Controller;
use blog\manage\PhotoManageService;
use \Yii;
use blog\entities\Photo;
use yii\web\NotFoundHttpException;

class PhotoController extends Controller
{
	private $service;

	public function behaviors()
	{
		return [
			'verb' => [
				'class' => VerbFilter::class,
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function __construct( $id, $module, array $config = [], PhotoManageService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	public function actionCreate()
	{
		$form = new PhotoCreateForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$this->service->addPhotos( $form );
				Yii::$app->session->setFlash( 'success', 'photo uploaded' );
				return $this->redirect(['photo/create']);
			} catch ( \RuntimeException $e ) {
				Yii::$app->errorHandler->logException( $e );
				Yii::$app->session->setFlash( 'error' , $e->getMessage() );
			}
		}

		return $this->render('create', ['model' => $form]);

	}

	/**
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PhotoSearchForm();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel'=> $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}


	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id)
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	public function actionUpdate($id)
	{
		$photo = $this->findModel($id);
		$form = new PhotoEditForm($photo);

		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->editPhoto( $photo->id, $form );
				return $this->redirect(['view', 'id' => $id]);
			}catch (\RuntimeException $e){
				Yii::$app->errorHandler->logException( $e );
				Yii::$app->session->setFlash( 'error' , $e->getMessage() );
			}
		}

		return $this->render('update', [
			'model' => $form,
			'photo' => $photo
		]);
	}

	/**
	 * @param $id
	 * @return Photo the loaded model
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = Photo::findOne($id)) !== null){
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}