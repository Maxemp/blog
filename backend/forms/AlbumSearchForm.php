<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 19.03.2018
 * Time: 17:04
 */

namespace backend\forms;


use blog\entities\Album;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class AlbumSearchForm extends Model
{
	public $id;
	public $name;

	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['name'], 'string'],
		];
	}

	public function search($params)
	{
		$query = Album::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()){
			$query->where('0=1');
			return $dataProvider;
		}

		$query
		->andFilterWhere(['like', 'id', $this->id])
		->andFilterWhere(['like', 'name', $this->name]);

		return $dataProvider;
	}
}