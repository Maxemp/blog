<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.03.2018
 * Time: 20:00
 */

namespace backend\forms;


use yii\base\Model;
use blog\entities\Photo;
use yii\data\ActiveDataProvider;

class PhotoSearchForm extends Model
{
	public $id;
	public $file;
	public $album_id;

	public function rules()
	{
		return[
			[['id'], 'integer'],
			[['album_id'], 'integer'],
			[['file'], 'string'],
		];
	}

	public function search($params)
	{
		$query = Photo::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if(!$this->validate()){
			$query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere(['like', 'id', $this->id])
			->andFilterWhere(['like', 'file', $this->file])
			->andFilterWhere(['like', 'album_id', $this->album_id]);

		return $dataProvider;
	}
}