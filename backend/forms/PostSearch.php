<?php

namespace backend\forms;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use blog\entities\Post;

/**
 * PostSearch represents the model behind the search form of `\blog\entities\Post`.
 */
class PostSearch extends Model
{
	public $title;
	public $id;
	public $date;
	public $created_by;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'created_by'], 'safe'],
	        [['date'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
	        ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'created_at', $this->date ? strtotime($this->date) : null]);

        return $dataProvider;
    }
}
