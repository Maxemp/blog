<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use blog\helpers\PostHelper;

/* @var $this yii\web\View */
/* @var $model blog\entities\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <div class="box box-default">
        <div class="box-body">
            <p>
		        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
			        'class' => 'btn btn-danger',
			        'data' => [
				        'confirm' => 'Are you sure you want to delete this item?',
				        'method' => 'post',
			        ],
		        ]) ?>
            </p>

	        <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
			        'id',
			        'title',
			        'created_at:date',
			        'description',
			        [
				        'attribute' => 'content',
				        'format' => 'raw'
			        ],
                    [
                        'attribute' => 'created_by',
                        'value' => PostHelper::authorName($model->created_by),
                    ],
		        ],
	        ]) ?>
        </div>
    </div>


</div>
