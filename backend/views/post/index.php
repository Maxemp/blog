<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;
use blog\entities\Post;
use blog\helpers\PostHelper;
/* @var $this yii\web\View */
/* @var $searchModel backend\forms\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <p>
		<?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box box-default">
        <div class="box-body">
	        <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],

			        'id',
			        'title',
			        [
				        'attribute' => 'created_at',
				        'filter' => DatePicker::widget([
					        'model' => $searchModel,
					        'attribute' => 'date',
					        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
					        'separator' => '-',
					        'pluginOptions' => [
						        'todayHighlight' => true,
						        'autoclose' => true,
						        'format' => 'yyyy-mm-dd'
					        ]
				        ]),
				        'format' => 'date'
			        ],
			        [
				        'attribute' => 'created_by',
				        'filter' => PostHelper::authorList(),
				        'value' => function (Post $post){
					        return PostHelper::authorName($post->created_by);
				        },
				        'format' => 'raw',
			        ],

			        ['class' => 'yii\grid\ActionColumn'],
		        ],
	        ]); ?>
        </div>
    </div>




</div>
