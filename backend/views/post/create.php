<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model blog\forms\manage\Post\PostCreateForm */

$this->title = 'Create Post';
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin()?>
            <?= $form->field($model, 'title')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'description')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'content')->widget(CKEditor::className(),[

                'editorOptions' => ElFinder::ckeditorOptions('elfinder', [])
            ]);?>
            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
            </div>
            <?php ActiveForm::end()?>
        </div>
    </div>

</div>
