<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model blog\forms\manage\document\DocumentEditForm */

$this->title = 'Update Document: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="document-update">
    <?php $form = ActiveForm::begin()?>
    <div class="box box-body">
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'number')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'date')->widget(DatePicker::class, [
	            'model' => $model,
	            'attribute' => 'date',
	            'separator' => '-',
	            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
	            'pluginOptions' => [
		            'format' => 'yyyy-mm-dd',
	            ]])?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end()?>
</div>
