<?php



/* @var $this yii\web\View */
/* @var $model blog\forms\manage\document\DocumentCreateForm */

use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;
use yii\helpers\Html;
$this->title = 'Create Document';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false])?>
    <div class="box box-default">
        <div class="box-body">
            <?= $form->field($model, 'number')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>
            <?= $form->field($model, 'date')->widget(DatePicker::class, [
                    'model' => $model,
                    'attribute' => 'date',
                    'separator' => '-',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                    ]
            ])?>
	        <?= $form->field($model, 'document')->widget(FileInput::class, [
		        'model' => $model,
		        'attribute' => 'document'
	        ])
	        ?>
        </div>
    </div>
    <div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
    </div>
	<?php ActiveForm::end()?>
</div>
