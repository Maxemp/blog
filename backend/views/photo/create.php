<?php

/* @var $model blog\forms\manage\Photo\PhotoCreateForm */
/* @var $this yii\web\View */

$this->title = 'Upload Photo';
$this->params['breadcrumbs'][] = $this->title;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
?>
<div class="box box-default">
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false]) ?>
    <div class="box-body">
		<?= $form->field($model, 'files[]')->widget(FileInput::class, [
			'options' => [
				'accept' => 'image/*',
				'multiple' => true,
			]
		])
		?>
    </div>
	<?php ActiveForm::end();?>
</div>
