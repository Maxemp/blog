<?php
/* @var $this yii\web\View */
/* @var $model blog\entities\Photo*/
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\widgets\DetailView;
use yii\helpers\Html;
?>
<p>
	<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
		'class' => 'btn btn-danger',
		'data' => [
			'confirm' => 'Are you sure you want to delete this item?',
			'method' => 'post',
		],
	]) ?>
</p>
<div class="user-view">
	<div class="box">
		<div class="box-body">
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					'id',
					'file',
                    'album_id',
				],
			])?>
			<?=
				Html::img($model->getImageFileUrl('file'))
			?>
		</div>
	</div>
</div>
