<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 08.04.2018
 * Time: 19:17
 */
/* @var $this yii\web\View */
/* @var $model blog\forms\manage\photo\PhotoEditForm */
/* @var $photo blog\entities\Photo */

$this->title = 'Update Photo';
$this->params['breadcrumbs'][] = ['label' => 'Photos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $photo->id, 'url' => ['view', 'id' => $photo->id]];
$this->params['breadcrumbs'][] = 'Update';

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use blog\helpers\PhotoHelper;
?>

<div class="user-update">

	<?php $form = ActiveForm::begin();?>

        <?=
        Html::img($photo->getImageFileUrl('file'))
        ?>
        <?= $form->field($model, 'album_id')->dropDownList(PhotoHelper::albumsList(), ['prompt' => ''])?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
        </div>

	<?php ActiveForm::end();?>

</div>
