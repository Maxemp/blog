<?php
/**
 * @var $this yii\web\View
 * @var $searchModel backend\forms\PhotoSearchForm
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use yii\helpers\Html;
use yii\grid\GridView;
use blog\entities\Photo;
use yii\grid\ActionColumn;
use blog\helpers\PhotoHelper;

$this->title = 'Photos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<p>
		<?= Html::a('Upload Photo', ['create'], ['class' => 'btn btn-success'])?>
	</p>

	<div class="box">
		<div class="box-body">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					[
						'value' => function(Photo $model){
							return Html::img($model->getThumbFileUrl('file', 'admin'));
						},
						'format' => 'raw',
						'contentOptions' => ['style' => 'width: 100px']
					],
					'id',
					[
						'attribute' => 'album_id',
						'filter' => PhotoHelper::albumsList(),
						'value' => function (Photo $photo){
							return PhotoHelper::albumName($photo->album_id);
						},
						'format' => 'raw',
					],
                    'file',
                    ['class' => ActionColumn::class]
				],
			]);
			?>
		</div>
	</div>
</div>
