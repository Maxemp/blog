<?php if (Yii::$app->user->can('admin')):?>
    <aside class="main-sidebar">

        <section class="sidebar">

                <?= dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Управление', 'options' => ['class' => 'header']],
                            ['label' => 'Пользователи', 'url' => ['/user/index'], 'active' => $this->context->id == 'user', 'icon' => 'user'],
                            ['label' => 'Фотографии', 'url' => ['/photo/index'], 'active' => $this->context->id == 'photo'],
                            ['label' => 'Фотоальбомы', 'url' => ['/album/index'], 'active' => $this->context->id == 'album'],
                            ['label' => 'Видео', 'url' => ['/video/index'], 'active' => $this->context->id == 'video'],
                            ['label' => 'Документы', 'url' => ['/document/index'], 'active' => $this->context->id == 'document'],
                            ['label' => 'Календарные планы', 'url' => ['/cplan/index'], 'active' => $this->context->id == 'cplan'],
                            ['label' => 'Положения', 'url' => ['/provision/index'], 'active' => $this->context->id == 'provision'],
                            ['label' => 'Результаты', 'url' => ['/result/index'], 'active' => $this->context->id == 'result'],
                            ['label' => 'Статьи', 'url' => ['/post/index'], 'active' => $this->context->id == 'post'],
                            ['label' => 'Комментарии', 'url' => ['/comment/index'], 'active' => $this->context->id == 'comment'],
                        ],
                    ]
                ) ?>

        </section>

    </aside>
<?php endif;?>