<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model blog\forms\manage\competition\ProvisionCreateForm */

$this->title = 'Create Provision';
$this->params['breadcrumbs'][] = ['label' => 'Provisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provision-create">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false])?>

            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date')->widget(DatePicker::class, [
	            'model' => $model,
	            'attribute' => 'date',
	            'separator' => '-',
	            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
	            'pluginOptions' => [
		            'format' => 'yyyy-mm-dd',
	            ]
            ])?>

            <?= $form->field($model, 'url')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'file')->widget(FileInput::class, [
	            'model' => $model,
	            'attribute' => 'file'
            ])
            ?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
            </div>

            <?php ActiveForm::end()?>
        </div>
    </div>
</div>
