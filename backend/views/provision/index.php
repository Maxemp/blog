<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\ProvisionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Provisions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provision-index">
    <div class="box box-default">
        <div class="box-body">
            <p>
		        <?= Html::a('Create Provision', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

	        <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],

			        'id',
			        [
				        'attribute' => 'date',
				        'filter' => DatePicker::widget([
					        'model' => $searchModel,
					        'attribute' => 'date',
					        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
					        'separator' => '-',
					        'pluginOptions' => [
						        'todayHighlight' => true,
						        'autoclose' => true,
						        'format' => 'yyyy-mm-dd'
					        ]
				        ]),
				        'format' => 'date'
			        ],
			        'name',

			        ['class' => 'yii\grid\ActionColumn'],
		        ],
	        ]); ?>
        </div>
    </div>
</div>
