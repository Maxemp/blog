<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model blog\forms\manage\competition\ProvisionEditForm */
/* @var $provision \blog\entities\competition\Provision*/

$this->title = 'Update Provision:'.$provision->name;
$this->params['breadcrumbs'][] = ['label' => 'Provisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $provision->name, 'url' => ['view', 'id' => $provision->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="provision-update">

    <div class="box box-default">
        <div class="box-body">

            <?php $form = ActiveForm::begin()?>
            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'url')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date')->widget(DatePicker::class, [
	            'model' => $model,
	            'attribute' => 'date',
	            'separator' => '-',
	            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
	            'pluginOptions' => [
		            'format' => 'yyyy-mm-dd',
	            ]
            ])?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
            </div>

            <?php ActiveForm::end()?>

        </div>
    </div>

</div>
