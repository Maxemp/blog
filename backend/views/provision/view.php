<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model blog\entities\competition\Provision */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Provisions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provision-view">
    <div class="box box-default">
        <div class="box-body">
            <p>
		        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		        <?= Html::a('Download', ['download', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
		        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
			        'class' => 'btn btn-danger',
			        'data' => [
				        'confirm' => 'Are you sure you want to delete this item?',
				        'method' => 'post',
			        ],
		        ]) ?>
            </p>

	        <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
			        'id',
			        'date',
			        'name',
			        'file',
			        'url:url',
		        ],
	        ]) ?>
        </div>
    </div>


</div>
