<?php

use yii\helpers\Html;
use yii\grid\GridView;
use blog\entities\Video;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">
    <p>
		<?= Html::a('Create Video', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
	        <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'large_thumbnail',
                        'value' => function (Video $model){
	                        return Html::img($model->large_thumbnail);
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px']
                    ],
                    'name',
			        'id',
			        'service',
			        'url:url',
			        [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {delete}',
                    ],
		        ],
	        ]); ?>
        </div>
    </div>



</div>
