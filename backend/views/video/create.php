<?php



/* @var $this yii\web\View */
/* @var $model blog\entities\Video */

$this->title = 'Create Video';
$this->params['breadcrumbs'][] = ['label' => 'Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
