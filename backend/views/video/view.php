<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model blog\entities\Video */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-view">

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="box">
        <div class="box-body">
	        <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
			        'id',
			        'service',
			        'url:url',
			        [
				        'attribute' => 'embed_code',
				        'format' => 'raw'
			        ]
		        ],
	        ]) ?>
        </div>
    </div>


</div>
