<?php
/* @var $model blog\forms\manage\album\AlbumCreateForm */
/* @var $this yii\web\View */

$this->title = 'Create Album';
$this->params['breadcrumbs'][] = $this->title;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Html;
?>
<div class="box box-default">
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false]);?>
	<div class="box-body">
		<?= $form->field($model, 'name')->textInput(['maxLength' => true])?>
		<?= $form->field($model->photos, 'files[]')->widget(FileInput::class, [
			'options' => [
				'accept' => 'image/*',
				'multiple' => true,
			]
		])
		?>
	</div>
</div>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
</div>
<?php ActiveForm::end();?>


