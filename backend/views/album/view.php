<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 14.04.2018
 * Time: 14:42
 */

/* @var $this yii\web\View */
/* @var $model blog\entities\Album */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Album', 'url'=> ['index']];
$this->params['breadcrumbs'][] = $this->title;
use yii\helpers\Html;
use yii\widgets\DetailView;
use blog\helpers\AlbumHelper;
$photos = AlbumHelper::getPhotos($model->id);
?>

<p>
	<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
		'class' => 'btn btn-danger',
		'data' => [
			'confirm' => 'Are you sure you want to delete this item?',
			'method' => 'post',
		],
	]) ?>
</p>

<div class="user-view">
	<div class="box">
		<div class="box-body">
			<?=
			DetailView::widget([
				'model' => $model,
				'attributes' =>
					[
						'id',
						'name'
					]
			]);
			?>

			<?php foreach ($photos as $photo):?>
				<?=
				Html::img($photo->getImageFileUrl('file'), ['height' => '255', 'width' => '255'])
				?>
			<?php endforeach;?>
		</div>
	</div>

</div>
