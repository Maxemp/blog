<?php
/* @var $this yii\web\View */
/* @var $searchModel backend\forms\AlbumSearchForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
$this->title = 'Albums';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
	<p>
		<?= Html::a('Create Album', ['create'], ['class' => 'btn btn-success'])?>
	</p>

	<div class="box">
		<div class="box-body">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					'id',
					'name',
					['class' => ActionColumn::class]
				],
			]);
			?>
		</div>
	</div>
</div>
