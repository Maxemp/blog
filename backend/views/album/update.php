<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 14.04.2018
 * Time: 20:18
 */
/* @var $this yii\web\View */
/* @var $model blog\forms\manage\album\AlbumEditForm*/
/* @var $album  blog\entities\Album*/
$this->title = $album->id;
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $album->id, 'url' => ['view', 'id' => $album->id]];
$this->params['breadcrumbs'][] = 'Update';
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\FileInput;
?>

<div class="user-update">
	<?php $form = ActiveForm::begin()?>
		<?=$form->field($model, 'name')->textInput(['maxLength' => true]);?>
        <?= $form->field($model->photos, 'files[]')->widget(FileInput::class, [
            'options' => [
                'accept' => 'image/*',
                'multiple' => true,
            ]
        ])
        ?>
		<div class="form-group">
			<?=
				Html::submitButton('Save', ['class' => ['btn btn-success']]);
			?>
		</div>
	<?php ActiveForm::end()?>
</div>
