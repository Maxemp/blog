<?php

use blog\helpers\UserHelper;
use kartik\date\DatePicker;
use blog\entities\User\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\UserSearchForm */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        'id',
			        [
                        'attribute' => 'created_at',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_from',
                            'attribute2' => 'date_to',
                            'type' => DatePicker::TYPE_RANGE,
                            'separator' => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]),
                        'format' => 'datetime'
                    ],
			        'username',
			        'email:email',
			        [
                        'attribute' => 'status',
                        'filter' => UserHelper::statusList(),
                        'value' => function (User $user){
                            return UserHelper::statusLabel($user->status);
                        },
                        'format' => 'raw',
                    ],
			        ['class' => ActionColumn::class],
		        ],
	        ]); ?>
        </div>
    </div>
</div>
