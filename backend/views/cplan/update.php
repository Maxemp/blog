<?php
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use blog\helpers\CPlanHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model blog\forms\manage\competition\CplanEditForm */
/* @var $cplan \blog\entities\competition\CPlan */

$this->title = 'Update Cplan: '.$cplan->name;
$this->params['breadcrumbs'][] = ['label' => 'Cplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $cplan->name, 'url' => ['view', 'id' => $cplan->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="cplan-update">
    <div class="box box-body">
        <div class="box-body">
	        <?php $form = ActiveForm::begin()?>

            <?= $form->field($model, 'date')->widget(DatePicker::class, [
                'model' => $model,
                'attribute' => 'date',
                'separator' => '-',
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ])?>

            <?= $form->field($model, 'age_group')->dropDownList(CPlanHelper::ageList())?>

            <?= $form->field($model, 'name')->textInput(['maxLength' =>true])?>

            <?= $form->field($model, 'country')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'city')->textInput(['maxLength' => true])?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
            </div>

            <?php ActiveForm::end()?>
        </div>
    </div>
</div>
