<?php

use yii\helpers\Html;
use yii\grid\GridView;
use blog\helpers\CPlanHelper;
use blog\entities\competition\CPlan;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\CPlanSearchForm */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cplans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cplan-index">

    <p>
        <?= Html::a('Create Cplan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box box-body">
        <div class="box-body">
	        <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],

			        'id',
			        [
				        'attribute' => 'date',
				        'filter' => DatePicker::widget([
					        'model' => $searchModel,
					        'attribute' => 'date',
					        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
					        'separator' => '-',
					        'pluginOptions' => [
						        'todayHighlight' => true,
						        'autoclose' => true,
						        'format' => 'yyyy-mm-dd'
					        ]
				        ]),
				        'format' => 'date'
			        ],
			        [
				        'attribute' => 'age_group',
				        'filter' => CPlanHelper::ageList(),
				        'value' => function (CPlan $c_plan){
					        return CPlanHelper::ageName($c_plan->age_group);
				        },
				        'format' => 'raw',
			        ],
			        'name',
			        'country',
			        'city',

			        ['class' => 'yii\grid\ActionColumn'],
		        ],
	        ]); ?>
        </div>
    </div>

</div>
