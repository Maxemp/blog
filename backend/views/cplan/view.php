<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use blog\helpers\CPlanHelper;

/* @var $this yii\web\View */
/* @var $model blog\entities\competition\CPlan */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cplan-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="box box-body">
        <div class="box-body">
	        <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
			        'id',
			        'date',
			        [
				        'attribute' => 'age_group',
				        'value' => CPlanHelper::ageName($model->age_group),
				        'format' => 'raw',
			        ],
			        'name',
			        'country',
			        'city',
		        ],
	        ]) ?>
        </div>
    </div>
</div>
