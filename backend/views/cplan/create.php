<?php
/* @var $this yii\web\View */
/* @var $model \blog\forms\manage\competition\CplanCreateForm */

$this->title = 'Create Cplan';
$this->params['breadcrumbs'][] = ['label' => 'Cplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
use blog\helpers\CPlanHelper;
?>
<div class="cplan-create">
    <div class="box box-body">
        <div class="box-body">
	        <?php $form = ActiveForm::begin()?>

            <?= $form->field($model, 'date')->widget(DatePicker::class, [
		        'model' => $model,
		        'attribute' => 'date',
		        'separator' => '-',
		        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
		        'pluginOptions' => [
			        'format' => 'yyyy-mm-dd',
		        ]
	        ])?>

            <?= $form->field($model, 'age_group')->dropDownList(CPlanHelper::ageList(), ['prompt' => ''])?>

            <?= $form->field($model, 'name')->textInput(['maxLength' => true]) ?>

            <?= $form->field($model, 'country')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'city')->textInput(['maxLength' => true]) ?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-primary'])?>
            </div>

            <?php ActiveForm::end()?>
        </div>
    </div>


</div>
