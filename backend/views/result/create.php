<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model blog\forms\manage\competition\ResultCreateForm */

$this->title = 'Create Result';
$this->params['breadcrumbs'][] = ['label' => 'Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="result-create">

    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'enableClientValidation' => false])?>

            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date_from')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date_to')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'country')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'city')->textInput(['maxLength' => true])?>

	        <?= $form->field($model, 'file')->widget(FileInput::class, [
		        'model' => $model,
		        'attribute' => 'file'
	        ])
	        ?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
            </div>
            <?php ActiveForm::end()?>

        </div>
    </div>

</div>
