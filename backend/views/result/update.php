<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \blog\forms\manage\competition\ResultEditForm */
/* @var $result \blog\entities\competition\Result */

$this->title = 'Update Result: '.$result->name;
$this->params['breadcrumbs'][] = ['label' => 'Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $result->name, 'url' => ['view', 'id' => $result->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="result-update">

    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin()?>

            <?= $form->field($model, 'name')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date_from')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'date_to')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'country')->textInput(['maxLength' => true])?>

            <?= $form->field($model, 'city')->textInput(['maxLength' => true])?>

            <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success'])?>
            </div>

            <?php ActiveForm::end()?>
        </div>
    </div>

</div>
