<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\forms\ResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Results';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="result-index">
    <div class="box box-default">
        <div class="box-body">
            <p>
		        <?= Html::a('Create Result', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

	        <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],

			        'id',
			        'name',
			        'date_from',
			        'date_to',
			        'country',
			        'city',

			        ['class' => 'yii\grid\ActionColumn'],
		        ],
	        ]); ?>
        </div>
    </div>


</div>
