<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.11.2018
 * Time: 22:38
 *
 * @var $post \blog\entities\Post
 */
use dosamigos\disqus\Comments;
use blog\helpers\PostHelper;
?>
<div class="post-index">
	<div class="box box-default">
		<div class="box-body">
			<p><?= date("Y-m-d H:i:s", $post->created_at)?></p>
			<h1><?= $post->title?></h1>
			<p><?= $post->content?></p>

			<p><?= PostHelper::authorName($post->created_by)?></p>

            <?= Comments::widget([
                    'shortname' => 'blog-vzih21qazp',
                    'identifier' => $post->id
            ])?>
		</div>
	</div>
</div>