<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.11.2018
 * Time: 21:47
 *
 * @var $post \blog\entities\Post
 */
use blog\helpers\PostHelper;
use yii\helpers\Html;
?>
<div class="post-index">
	<div class="box box-default">
		<div class="box-body">
			<?php foreach ($post as $item):?>
				<h1>Title: <?= Html::a($item['title'], ['post/view', 'id' => $item['id']])?></h1>
				<p>Author: <?= PostHelper::authorName($item['created_by'])?></p>
			<?php endforeach;?>
		</div>
	</div>
</div>
