<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 07.02.2018
 * Time: 15:12
 */

namespace frontend\controllers;

use Yii;
use blog\forms\ContactForm;
use blog\services\contact\ContactService;
use yii\web\Controller;


class ContactController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], ContactService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	public function actionIndex()
	{
		$form = new ContactForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->send($form);
				Yii::$app->session->setFlash('success', 'Thank you for contacting us.');
				return $this->goHome();
			}catch (\RuntimeException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', 'Sending error');
			}
		}

		return $this->render('index', ['model' => $form]);
	}
}