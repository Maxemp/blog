<?php

namespace frontend\controllers\auth;

use blog\services\auth\AuthService;
use Yii;
use yii\web\Controller;
use blog\forms\auth\LoginForm;

class AuthController extends Controller
{
	private $service;

	public  function __construct( $id, $module, array $config = [], AuthService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	public function actionLogin()
	{
		if(!Yii::$app->user->isGuest){
			return $this->goHome();
		}

		$form = new LoginForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$user = $this->service->auth($form);
				Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
				return $this->goHome();
			}catch (\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render('login', ['model' => $form]);
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}
}