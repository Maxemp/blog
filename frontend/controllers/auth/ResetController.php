<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 04.02.2018
 * Time: 20:02
 */

namespace frontend\controllers\auth;

use yii\web\BadRequestHttpException;
use yii\web\Controller;
use blog\services\auth\PasswordResetService;
use blog\forms\auth\PasswordResetRequestForm;
use blog\forms\auth\ResetPasswordForm;
use Yii;

class ResetController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], PasswordResetService $service )
	{
		$this->service = $service;
		parent::__construct( $id, $module, $config );
	}

	public function actionRequest()
	{
		$form = new PasswordResetRequestForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->request($form);
				Yii::$app->session->setFlash('success', 'Check your email');
				return $this->goHome();
			}catch (\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}
		return $this->render('request', ['model' => $form]);
	}

	public function actionConfirm($token)
	{
		try{
			$this->service->validateToken($token);
		}catch (\DomainException $e){
			throw new BadRequestHttpException($e->getMessage());
		}
		$form = new ResetPasswordForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->reset($token, $form);
				Yii::$app->session->setFlash('success', 'New password saved.');
				return $this->goHome();
			}catch (\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				YIi::$app->session->setFlash('error', $e->getMessage());
			}
		}
		return $this->render('confirm', ['model' => $form]);
	}
}