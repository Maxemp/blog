<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 09.02.2018
 * Time: 2:50
 */

namespace frontend\controllers\auth;

use blog\services\auth\SignUpService;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use blog\forms\auth\SignupForm;


class SignupController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], SignUpService $service )
	{
		$this->service =$service;
		parent::__construct( $id, $module, $config );
	}

	public function behaviors():array
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			]
		];
	}

	public function actionRequest()
	{
		$form = new SignupForm();
		if ($form->load(Yii::$app->request->post()) && $form->validate()){
			try{
				$this->service->signup($form);
				Yii::$app->session->setFlash('success', 'Check your email');
				return $this->goHome();
			}catch (\DomainException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}

		return $this->render('request', ['model' => $form]);
	}

	public function actionConfirm($token)
	{
		try{
			$this->service->confirm($token);
			Yii::$app->session->setFlash('success', 'Your email is confirmed');
			return $this->redirect(['auth/auth/login']);
		}catch (\DomainException $e){
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());
		}
		return $this->goHome();
	}
}