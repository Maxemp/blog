<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.02.2018
 * Time: 17:38
 */

namespace frontend\controllers\cabinet;


use blog\services\auth\NetworkService;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class NetworkController extends Controller
{
	private $service;

	public function __construct( $id, $module, array $config = [], NetworkService $service )
	{
		parent::__construct( $id, $module, $config );
		$this->service = $service;
	}

	public function actions()
	{
		return [
			'attach' => [
				'class' => AuthAction::class,
				'successCallback' => [$this, 'onAuthSuccess'],
			]
		];
	}

	public function onAuthSuccess(ClientInterface $client):void
	{
		$network = $client->getId();
		$attributes = $client->getUserAttributes();
		$identity = ArrayHelper::getValue($attributes, 'id');

		try{
			$this->service->attach(Yii::$app->user->id ,$network, $identity);
			Yii::$app->session->setFlash('success', 'Network successfully attached');
		}catch(\DomainException $e){
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());
		}
	}
}