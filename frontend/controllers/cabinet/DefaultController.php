<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.02.2018
 * Time: 17:13
 */

namespace frontend\controllers\cabinet;


use yii\filters\AccessControl;
use yii\web\Controller;

class DefaultController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					]
				]
			]
		];
	}

	/**
	 * @return mixed
	 */
	public function actionIndex(){
		return $this->render('index');
	}

}