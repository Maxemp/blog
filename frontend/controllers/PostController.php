<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.11.2018
 * Time: 21:38
 */

namespace frontend\controllers;


use blog\entities\Post;
use yii\web\Controller;

class PostController extends Controller
{
	public function actionIndex()
	{
		$post = Post::find()->all();
		return $this->render('index', ['post' => $post]);
	}

	public function actionView($id)
	{
		$post = Post::findOne($id);
		return $this->render('view', [
			'post' => $post,
		]);
	}
}