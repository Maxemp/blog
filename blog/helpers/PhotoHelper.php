<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.04.2018
 * Time: 19:07
 */

namespace blog\helpers;

use blog\entities\Album;
use yii\helpers\ArrayHelper;

class PhotoHelper
{
	public static function albumsList():array
	{
		return ArrayHelper::map(Album::find()->asArray()->all(), 'id','name');
	}

	public static function albumName($album_id)
	{
		return ArrayHelper::getValue(self::albumsList(), $album_id);
	}

}