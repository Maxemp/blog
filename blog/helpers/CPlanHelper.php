<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 15:26
 */

namespace blog\helpers;


use yii\helpers\ArrayHelper;

class CPlanHelper
{
	/**
	 * @return array
	 */
	public static function ageList(): array
	{
		return [
			'0' => 'Взрослые',
			'1' => 'Молодежь',
			'2' => 'Юниоры',
			'3' => 'Юноши',
			'4' => 'Кадеты',
			'5' => 'Ветераны'
		];
	}

	public static function ageName($age):string
	{
		return ArrayHelper::getValue(self::ageList(), $age);
	}
}