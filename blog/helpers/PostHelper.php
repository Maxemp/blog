<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 28.06.2018
 * Time: 17:04
 */

namespace blog\helpers;


use blog\entities\User\User;
use yii\helpers\ArrayHelper;

class PostHelper
{

	public static function authorList()
	{
		return ArrayHelper::map(User::find()->asArray()->all(),'id', 'username');
	}

	public static function authorName($userId)
	{
		return ArrayHelper::getValue(self::authorList(), $userId);
	}

}