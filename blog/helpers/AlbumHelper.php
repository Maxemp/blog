<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 25.06.2018
 * Time: 18:11
 */

namespace blog\helpers;


use blog\entities\Photo;

class AlbumHelper
{

	public static function getPhotos($album_id):array
	{
		return  Photo::find()->where(['album_id' => $album_id])->all();
	}

}