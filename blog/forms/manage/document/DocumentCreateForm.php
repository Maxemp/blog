<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 30.04.2018
 * Time: 14:21
 */

namespace blog\forms\manage\document;


use yii\base\Model;
use yii\web\UploadedFile;

class DocumentCreateForm extends Model
{
	/* @var UploadedFile */
	public $document;

	/* @var string */
	public $number;

	/* @var string*/
	public $name;

	/* @var string */
	public $date;

	public function rules()
	{
		return [
			[['document'], 'file'],
			[['name', 'number', 'date'], 'string', 'max' => 255],
			[['name', 'number', 'date'], 'required'],
		];
	}

	public function beforeValidate(): bool
	{
		if (parent::beforeValidate()) {
			$this->document = UploadedFile::getInstance($this, 'document');
			return true;
		}
		return false;
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'number' => '№',
			'name' => 'Название Документа',
			'date' => 'Дата публикации',
			'document' => 'Документ'
		];
	}

}