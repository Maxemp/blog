<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 30.04.2018
 * Time: 14:26
 */

namespace blog\forms\manage\document;


use blog\entities\Document;
use yii\base\Model;

class DocumentEditForm extends Model
{
	/* @var integer */
	public $id;

	/* @var string */
	public $number;

	/* @var string */
	public $name;

	/* @var string */
	public $date;

	public function __construct( Document $doc, array $config = [])
	{
		$this->id = $doc->id;
		$this->number = $doc->number;
		$this->name = $doc->name;
		$this->date = $doc->date;
		parent::__construct( $config );
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			[['number', 'name', 'date'], 'string', 'max' => 255],
			[['number', 'name', 'date'], 'required'],
		];
	}
}