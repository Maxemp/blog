<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 05.03.2018
 * Time: 17:27
 */

namespace blog\forms\manage\Photo;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PhotoForm
 * @package blog\forms
 */
class PhotoCreateForm extends Model
{
	/**
	 * @var UploadedFile[]
	 */
	public $files;

	public function rules()
	{
		return [
			['files', 'each', 'rule' => ['image']],
		];
	}

	public function beforeValidate(): bool
	{
		if (parent::beforeValidate()) {
			$this->files = UploadedFile::getInstances($this, 'files');
			return true;
		}
		return false;
	}
}