<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 20.03.2018
 * Time: 14:29
 */

namespace blog\forms\manage\Photo;


use yii\base\Model;
use blog\entities\Photo;

class PhotoEditForm extends Model
{
	public $album_id;

	public function __construct( Photo $photo, array $config = [] )
	{
		$this->album_id = $photo->album_id;
		parent::__construct( $config );
	}

	public function rules()
	{
		return [
			[['album_id'], 'string']
		];
	}
}