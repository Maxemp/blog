<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 16.02.2018
 * Time: 22:24
 */

namespace blog\forms\manage\User;

use blog\entities\User\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class UserCreateForm extends Model
{
	public $username;
	public $email;
	public $password;
	public $role;

	public function rules()
	{
		return [
			[['username', 'password', 'role'], 'required'],
			['email', 'email'],
			[['username', 'email'], 'string', 'max' => 255],
			[['username', 'email'], 'unique', 'targetClass' => User::class],
			['password', 'string', 'min' => 5],
		];
	}

	public static function rolesList():array
	{
		return ArrayHelper::map(\Yii::$app->authManager->getRoles(), 'name', 'description');
	}
}