<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.06.2018
 * Time: 21:00
 */

namespace blog\forms\manage\Post;


use blog\entities\Post;
use yii\base\Model;

class PostEditForm extends Model
{

	public $title;
	public $description;
	public $content;

	public function __construct(Post $post, array $config = [] )
	{
		$this->title = $post->title;
		$this->description = $post->description;
		$this->content = $post->content;
		parent::__construct( $config );
	}

	public function rules()
	{
		return [
			[['title', 'description', 'content'], 'string'],
			[['title', 'description', 'content'], 'required'],
		];
	}

	public function attributeLabels()
	{
		return [
			'title' => 'Заголовок',
			'description'=> 'Описание',
			'content' => 'Контент'
		];
	}

}