<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.03.2018
 * Time: 13:45
 */

namespace blog\forms\manage\album;


use blog\forms\CompositeFrom;
use blog\forms\manage\Photo\PhotoCreateForm;

/* @property PhotoCreateForm $photos */
class AlbumCreateForm extends CompositeFrom
{
	public $name;

	public function __construct( $config = [] )
	{
		$this->photos = new PhotoCreateForm();
		parent::__construct( $config );
	}

	public function rules() :array
	{
		return [
			[['name'], 'required'],
			[['name'], 'string', 'max' => 255],
		];
	}

	protected function internalFrom(): array
	{
		return ['photos'];
	}

}