<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 14.04.2018
 * Time: 18:48
 */

namespace blog\forms\manage\album;


use blog\entities\Album;
use blog\forms\CompositeFrom;
use blog\forms\manage\Photo\PhotoCreateForm;

/* @property PhotoCreateForm $photos */
class AlbumEditForm extends CompositeFrom
{
	public $name;


	public function __construct( Album $album, array $config = [] )
	{
		$this->name = $album->name;
		$this->photos = new PhotoCreateForm();
		parent::__construct( $config );
	}

	public function rules()
	{
		return
		[
			['name', 'required'],
			['name', 'string']
		];
	}

	protected function internalFrom(): array
	{
		return ['photos'];
	}
}