<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.06.2018
 * Time: 17:34
 */

namespace blog\forms\manage\competition;


use blog\entities\competition\Provision;
use yii\base\Model;

class ProvisionEditForm extends Model
{
	public $name;
	public $date;
	public $url;

	public function __construct(Provision $provision, array $config = [] )
	{
		$this->name = $provision->name;
		$this->date = $provision->date;
		$this->url = $provision->url;
		parent::__construct( $config );
	}

	public function rules()
	{
		return [
			[['name', 'date', 'url'], 'string', 'max' => 255],
			[['name', 'date', 'url'], 'required'],
		];
	}

	public function attributeLabels()
	{
		return [
			'name' => 'Название документа',
			'date' => 'Дата публикации',
			'url' => 'Ссылка на скачивание'
		];
	}

}