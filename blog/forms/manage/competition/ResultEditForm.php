<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.06.2018
 * Time: 23:30
 */

namespace blog\forms\manage\competition;


use blog\entities\competition\Result;
use yii\base\Model;

class ResultEditForm extends Model
{
	public $date_from;
	public $date_to;
	public $name;
	public $country;
	public $city;

	public function __construct(Result $result, array $config = [] )
	{
		$this->date_from = $result->date_from;
		$this->date_to = $result->date_to;
		$this->name = $result->name;
		$this->country = $result->country;
		$this->city = $result->city;
		parent::__construct( $config );
	}

	public function rules()
	{
		return [
			[['date_to', 'date_from', 'name', 'city', 'country'], 'string', 'max' => 255],
			[['name', 'city', 'country'], 'required'],
		];
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'name' => 'Название турнира',
			'date_from' => 'Начало турнира',
			'date_to' => 'Конец турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}
}