<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.06.2018
 * Time: 23:29
 */

namespace blog\forms\manage\competition;


use yii\base\Model;
use yii\web\UploadedFile;

class ResultCreateForm extends Model
{

	public $date_from;
	public $date_to;
	public $name;
	public $country;
	public $city;
	/* @var UploadedFile */
	public $file;

	public function rules()
	{
		return [
			[['file'], 'file'],
			[['date_to', 'date_from', 'name', 'city', 'country'], 'string', 'max' => 255],
			[['name', 'city', 'country'], 'required'],
		];
	}

	public function beforeValidate(): bool
	{
		if (parent::beforeValidate()) {
			$this->file = UploadedFile::getInstance($this, 'file');
			return true;
		}
		return false;
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'file' => 'Файл',
			'name' => 'Название турнира',
			'date_from' => 'Начало турнира',
			'date_to' => 'Конец турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}

}