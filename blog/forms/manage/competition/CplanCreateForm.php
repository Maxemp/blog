<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 14:56
 */

namespace blog\forms\manage\competition;


use yii\base\Model;

class CplanCreateForm extends Model
{
	/* @var string */
	public $date;

	/* @var string */
	public $age_group;

	/* @var string */
	public $name;

	/* @var string */
	public $country;

	/* @vat string */
	public $city;

	public function rules():array
	{
		return [
			[['date', 'age_group', 'name', 'country', 'city'], 'string', 'max' => 255],
			[['date', 'age_group', 'name', 'country', 'city'], 'required'],
		];
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'date' => 'Даты проведения',
			'age_group' => 'Возрастная группа',
			'name' => 'Название турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}

}