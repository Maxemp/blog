<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.06.2018
 * Time: 17:34
 */

namespace blog\forms\manage\competition;


use yii\base\Model;
use yii\web\UploadedFile;

class ProvisionCreateForm extends Model
{

	public $name;
	public $date;
	public $url;
	/**
	 * @var UploadedFile
	 */
	public $file;

	public function rules()
	{
		return [
			[['name', 'date', 'url'], 'string'],
			[['name', 'date'], 'required'],
			[['file'], 'file']
		];
	}

	public function beforeValidate(): bool
	{
		if (parent::beforeValidate()) {
			$this->file = UploadedFile::getInstance($this, 'file');
			return true;
		}
		return false;
	}

	public function attributeLabels()
	{
		return [
			'name' => 'Название документа',
			'date' => 'Дата публикации',
			'url' => 'Ссылка на скачивание',
			'file' => 'файл',
		];
	}
}