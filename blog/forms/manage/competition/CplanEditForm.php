<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 14:57
 */

namespace blog\forms\manage\competition;


use yii\base\Model;
use blog\entities\competition\CPlan;

class CplanEditForm extends Model
{
	/* @var string */
	public $date;

	/* @var string */
	public $age_group;

	/* @var string */
	public $name;

	/* @var string */
	public $country;

	/* @vat string */
	public $city;

	public function __construct(Cplan $cplan, array $config = [] )
	{
		$this->date = $cplan->date;
		$this->age_group = $cplan->age_group;
		$this->name = $cplan->name;
		$this->country = $cplan->country;
		$this->city = $cplan->city;
		parent::__construct( $config );
	}

	/**
	 * @return array
	 */
	public function rules():array
	{
		return [
			[['date', 'age_group', 'name', 'country', 'city'], 'string', 'max' => 255],
			[['date', 'age_group', 'name', 'country', 'city'], 'required'],
		];
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'id' => 'ID',
			'date' => 'Даты проведения',
			'age_group' => 'Возрастная группа',
			'name' => 'Название турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}

}