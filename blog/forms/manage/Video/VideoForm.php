<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 22.04.2018
 * Time: 18:58
 */

namespace blog\forms\manage\Video;


use blog\entities\Video;

/**
 * Class VideoForm
 * @package blog\forms\manage\Video
 * @property integer $id
 * @property string $embed_code
 * @property string $service
 * @property string $large_thumbnail
 * @property string $url
 * @property string $name
 */
class VideoForm extends Video
{
	/**
	 * @return array
	 */
	public function rules()
	{
		return[
			[['url', 'name'], 'required'],
			[['embed_code', 'large_thumbnail', 'url', 'service', 'name'], 'string', 'max' => 255],
		];
	}
}