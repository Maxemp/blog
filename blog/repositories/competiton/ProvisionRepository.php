<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.06.2018
 * Time: 16:56
 */

namespace blog\repositories\competiton;


use blog\entities\competition\Provision;
use blog\repositories\NotFoundException;

class ProvisionRepository
{

	/**
	 * @param array $condition
	 *
	 * @return Provision
	 */
	private function getBy(array $condition):Provision
	{
		if(!$provision = Provision::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('Provision is not found');
		}
		return $provision;
	}

	/**
	 * @param $id
	 *
	 * @return Provision
	 */
	public function get($id):Provision
	{
		return $this->getBy(['id' => $id]);
	}

	/**
	 * @param Provision $provision
	 */
	public function save(Provision $provision):void
	{
		if (!$provision->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}