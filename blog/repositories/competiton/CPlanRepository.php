<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 15:15
 */

namespace blog\repositories\competiton;


use blog\entities\competition\CPlan;
use yii\web\NotFoundHttpException;
use \RuntimeException;

class CPlanRepository
{
	/**
	 * @param $id
	 *
	 * @return CPlan
	 * @throws NotFoundHttpException
	 */
	public function get($id):CPlan
	{
		if (!$c_plan = CPlan::findOne($id)){
			throw new NotFoundHttpException('Video is not found');
		}
		return $c_plan;
	}

	/**
	 * @param CPlan $c_plan
	 *
	 * @throws RuntimeException
	 */
	public function save(CPlan $c_plan):void
	{
		if (!$c_plan->save()){
			throw new RuntimeException('Saving error');
		}
	}

}