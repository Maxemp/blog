<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.06.2018
 * Time: 23:45
 */

namespace blog\repositories\competiton;


use blog\entities\competition\Result;
use blog\repositories\NotFoundException;

class ResultRepository
{

	/**
	 * @param array $condition
	 *
	 * @return Result
	 */
	private function getBy(array $condition):Result
	{
		if(!$result = Result::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('Provision is not found');
		}
		return $result;
	}

	/**
	 * @param $id
	 *
	 * @return Result
	 */
	public function get($id):Result
	{
		return $this->getBy(['id' => $id]);
	}

	/**
	 * @param Result $result
	 */
	public function save(Result $result):void
	{
		if (!$result->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}