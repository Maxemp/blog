<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 30.04.2018
 * Time: 14:06
 */

namespace blog\repositories;


use blog\entities\Document;
use PHPUnit\Framework\MockObject\RuntimeException;
use yii\web\NotFoundHttpException;

class DocumentRepository
{
	/**
	 * @param $id
	 *
	 * @return Document
	 * @throws NotFoundHttpException
	 */
	public function get($id):Document
	{
		if (!$document = Document::findOne($id)){
			throw new NotFoundHttpException('Video is not found');
		}
		return $document;
	}

	/**
	 * @param Document $document
	 *
	 * @throws RuntimeException
	 */
	public function save(Document $document):void
	{
		if (!$document->save()){
			throw new RuntimeException('Saving error');
		}
	}

}