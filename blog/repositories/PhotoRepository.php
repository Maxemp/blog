<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.03.2018
 * Time: 15:53
 */

namespace blog\repositories;


use blog\entities\Photo;

class PhotoRepository
{

	private function getBy(array $condition):Photo
	{
		if(!$photo = Photo::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('Photo is not found');
		}
		return $photo;
	}

	public function get($id):Photo
	{
		return $this->getBy(['id' => $id]);
	}

	public function getPhotoByAlbumId($album_id)
	{
		return Photo::findAll(['album_id' => $album_id]);
	}

	public function save(Photo $photo)
	{
		if (!$photo->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}