<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.04.2018
 * Time: 15:37
 */

namespace blog\repositories;


use blog\entities\Video;
use yii\web\NotFoundHttpException;

class VideoRepository
{

	/**
	 * @param $id
	 *
	 * @return Video
	 * @throws NotFoundHttpException
	 */
	public function get($id):Video
	{
		if (!$video = Video::findOne($id)){
			throw new NotFoundHttpException('Video is not found');
		}
		return $video;
	}

	/**
	 * @param Video $video
	 */
	public function save(Video $video):void
	{
		if (!$video->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}