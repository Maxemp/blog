<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 16.03.2018
 * Time: 18:48
 */

namespace blog\repositories;

use blog\entities\Album;

class AlbumRepository
{

	private function getBy(array $condition):Album
	{
		if(!$album = Album::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('Photo is not found');
		}
		return $album;
	}

	public function get($id):Album
	{
		return $this->getBy(['id' => $id]);
	}

	public function save(Album $album)
	{
		if (!$album->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}