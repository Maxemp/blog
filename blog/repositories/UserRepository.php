<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 04.02.2018
 * Time: 16:38
 */

namespace blog\repositories;

use blog\entities\User\User;

class UserRepository
{
	public function get($id):User
	{
		return $this->getBy(['id' => $id]);
	}

	public function findByNetworkIdentity($network, $identity): ?User
	{
		return User::find()->joinWith('network n')->andWhere(['n.network' => $network, 'n.identity' => $identity])->one();
	}

	public function getByEmailConfirmToken($token):User
	{
		return $this->getBy(['email_confirm_token' => $token]);
	}

	public function findByUsernameOrEmail($value): ?User
	{
		return User::find()->andWhere(['or', ['username' => $value], ['email' => $value]])->one();
	}

	public function getByEmail(string $email):User
	{
		return $this->getBy(['email' => $email]);
	}

	private function getBy(array $condition):User
	{
		if (!$user = User::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('User not found.');
		}
		return $user;
	}

	public function save(User $user):void
	{
		if (!$user->save()){
			throw new \RuntimeException('Saving Error');
		}
	}

	public function existsByPasswordResetToken(string  $token)
	{
		return (bool) User::findByPasswordResetToken($token);
	}

	public function getByPasswordResetToken(string $token):User
	{
		return $this->getBy(['password_reset_token' => $token]);
	}
}