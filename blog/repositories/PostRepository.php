<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.06.2018
 * Time: 21:07
 */

namespace blog\repositories;


use blog\entities\Post;

class PostRepository
{

	/**
	 * @param array $condition
	 *
	 * @return Post
	 */
	private function getBy(array $condition):Post
	{
		if(!$post = Post::find()->where($condition)->limit(1)->one()){
			throw new NotFoundException('Provision is not found');
		}
		return $post;
	}

	/**
	 * @param $id
	 *
	 * @return Post
	 */
	public function get($id):Post
	{
		return $this->getBy(['id' => $id]);
	}

	/**
	 * @param Post $post
	 */
	public function save(Post $post):void
	{
		if (!$post->save()){
			throw new \RuntimeException('Saving error');
		}
	}

}