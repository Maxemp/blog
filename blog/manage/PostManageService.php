<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.06.2018
 * Time: 20:57
 */

namespace blog\manage;


use blog\entities\Post;
use blog\forms\manage\Post\PostCreateForm;
use blog\forms\manage\Post\PostEditForm;
use blog\repositories\PostRepository;

class PostManageService
{

	private $repository;

	public function __construct(PostRepository $repository)
	{
		$this->repository = $repository;
	}

	public function create(PostCreateForm $form, $created_by):Post
	{
		$post = Post::create(
			$form->title,
			$form->description,
			$form->content,
			$created_by
		);

		$this->repository->save($post);
		return $post;
	}

	public function edit(PostEditForm $form, $id):void
	{
		$post = $this->repository->get($id);
		$post->edit(
			$form->title,
			$form->description,
			$form->content
		);
		$this->repository->save($post);
	}
}