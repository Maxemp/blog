<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 16.02.2018
 * Time: 22:20
 */

namespace blog\manage;

use blog\entities\User\User;
use blog\forms\manage\User\UserEditForm;
use blog\repositories\UserRepository;
use blog\forms\manage\User\UserCreateForm;
use yii\rbac\ManagerInterface;

class UserManageService
{

	public $repository;
	public $roles;

	public function __construct( UserRepository $repository, ManagerInterface $roles)
	{
		$this->roles = $roles;
		$this->repository = $repository;
	}

	public function create ( UserCreateForm $form): User
	{
		$user = User::create(
			$form->username,
			$form->email,
			$form->password
		);
		$this->repository->save($user);
		$this->roles->assign($form->role, $user->id);
		return $user;
	}

	public function edit($id, UserEditForm $form):void
	{
		$user = $this->repository->get($id);
		$user->edit(
			$form->username,
			$form->email
		);
		$this->repository->save($user);
	}
}