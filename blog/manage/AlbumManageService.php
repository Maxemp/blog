<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 16.03.2018
 * Time: 18:48
 */

namespace blog\manage;


use blog\forms\manage\album\AlbumEditForm;
use blog\repositories\AlbumRepository;
use blog\entities\Album;
use blog\repositories\PhotoRepository;
use blog\entities\Photo;
use blog\forms\manage\album\AlbumCreateForm;

class AlbumManageService
{

	private $albums;
	private $photos;

	public function __construct(AlbumRepository $albums, PhotoRepository $photos)
	{
		$this->photos = $photos;
		$this->albums = $albums;
	}

	public function create(AlbumCreateForm $form):Album
	{
		$album = Album::create($form->name);
		$this->albums->save($album);

		if ($form->photos) {
			foreach ($form->photos->files as $file){
				$photo = Photo::create($file, $album->id);
				$this->photos->save($photo);
			}
		}
		return $album;
	}

	public function edit($id, AlbumEditForm $form):void
	{
		$album = $this->albums->get($id);
		if ($form->photos) {
			foreach ($form->photos->files as $file){
				$photo = Photo::create($file, $album->id);
				$this->photos->save($photo);
			}
		}
		$album->edit($form->name);
		$this->albums->save($album);
	}

}