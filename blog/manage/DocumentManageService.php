<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 30.04.2018
 * Time: 14:05
 */

namespace blog\manage;


use blog\entities\Document;
use blog\forms\manage\document\DocumentCreateForm;
use blog\forms\manage\document\DocumentEditForm;
use blog\repositories\DocumentRepository;

class DocumentManageService
{
	/**
	 * @var DocumentRepository
	 */
	private $repository;

	public function __construct(DocumentRepository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @param DocumentCreateForm $form
	 *
	 * @return Document
	 */
	public function create(DocumentCreateForm $form):Document
	{
		$document = Document::create(
			$form->document,
			$form->number,
			$form->name,
			$form->date
		);

		$this->repository->save($document);
		return $document;
	}

	/**
	 * @param $id
	 * @param DocumentEditForm $form
	 */
	public function edit($id, DocumentEditForm $form):void
	{
		$document = $this->repository->get($id);
		$document->edit($form->number, $form->name, $form->date);
		$this->repository->save($document);
	}
}