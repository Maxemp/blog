<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.04.2018
 * Time: 15:42
 */

namespace blog\manage;


use blog\entities\Video;
use blog\repositories\VideoRepository;
use blog\forms\manage\Video\VideoForm;
use RicardoFiorani\Matcher\VideoServiceMatcher;
class VideoManageService
{
	/**
	 * @var VideoRepository
	 */
	private $video;

	/**
	 * @var VideoServiceMatcher
	 */
	private $matcher;

	public function __construct(VideoRepository $video, VideoServiceMatcher $matcher)
	{
		$this->video = $video;
		$this->matcher = $matcher;
	}

	public function create(VideoForm $form):Video
	{
		$match = $this->matcher->parse($form->url);

		$video = Video::create(
			$form->service = $match->getServiceName(),
			$form->embed_code = $match->getEmbedCode(400, 300),
			$form->url = $match->getRawUrl(),
			$form->large_thumbnail = $match->getSmallThumbnail(),
			$form->name
		);

		$this->video->save($video);
		return $video;
	}
}