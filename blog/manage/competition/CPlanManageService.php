<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 15:14
 */

namespace blog\manage\competition;


use blog\entities\competition\CPlan;
use blog\forms\manage\competition\CplanCreateForm;
use blog\forms\manage\competition\CplanEditForm;
use blog\repositories\competiton\CPlanRepository;

class CPlanManageService
{

	private $repository;

	public function __construct(CPlanRepository $repository)
	{
		$this->repository = $repository;
	}

	public function create(CplanCreateForm $form):CPlan
	{
		$c_plan = CPlan::create(
			$form->date,
			$form->age_group,
			$form->name,
			$form->country,
			$form->city
		);

		$this->repository->save($c_plan);
		return $c_plan;
	}

	public function edit($id, CplanEditForm $form):void
	{
		$c_plan = $this->repository->get($id);
		$c_plan->edit(
			$form->date,
			$form->age_group,
			$form->name,
			$form->country,
			$form->city
		);
		$this->repository->save($c_plan);
	}

}