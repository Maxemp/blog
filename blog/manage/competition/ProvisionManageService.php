<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.06.2018
 * Time: 16:55
 */

namespace blog\manage\competition;


use blog\entities\competition\Provision;
use blog\repositories\competiton\ProvisionRepository;
use blog\forms\manage\competition\ProvisionCreateForm;
use blog\forms\manage\competition\ProvisionEditForm;

class ProvisionManageService
{

	private $repository;

	public function __construct(ProvisionRepository $repository)
	{
		$this->repository = $repository;
	}

	public function create(ProvisionCreateForm $form):Provision
	{
		$provision = Provision::create(
			$form->file,
			$form->date,
			$form->name,
			$form->url
		);

		$this->repository->save($provision);
		return $provision;
	}

	public function edit(ProvisionEditForm $form, $id):void
	{
		$provision = $this->repository->get($id);
		$provision->edit(
			$form->name,
			$form->date,
			$form->url
		);
		$this->repository->save($provision);
	}

}