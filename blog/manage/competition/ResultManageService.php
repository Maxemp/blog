<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.06.2018
 * Time: 23:45
 */

namespace blog\manage\competition;


use blog\entities\competition\Result;
use blog\forms\manage\competition\ResultCreateForm;
use blog\forms\manage\competition\ResultEditForm;
use blog\repositories\competiton\ResultRepository;

class ResultManageService
{
	private $repository;

	public function __construct(ResultRepository $repository)
	{
		$this->repository = $repository;
	}

	public function create(ResultCreateForm $form):Result
	{
		$result = Result::create(
			$form->file,
			$form->name,
			$form->date_from,
			$form->date_to,
			$form->country,
			$form->city
		);

		$this->repository->save($result);
		return $result;
	}

	public function edit(ResultEditForm $form, $id):void
	{
		$result = $this->repository->get($id);
		$result->edit(
			$form->name,
			$form->date_to,
			$form->date_from,
			$form->country,
			$form->city
		);
		$this->repository->save($result);
	}

}