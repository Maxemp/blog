<?php

namespace blog\manage;

use blog\forms\manage\Photo\PhotoCreateForm;
use blog\repositories\PhotoRepository;
use blog\entities\Photo;
use blog\forms\manage\Photo\PhotoEditForm;
class PhotoManageService
{
	private $photos;

	public function __construct( PhotoRepository $photos )
	{
		$this->photos = $photos;
	}

	public function addPhotos(PhotoCreateForm $form):void
	{
		foreach($form->files as $file){
			$photo = Photo::create($file);
			$this->photos->save($photo);
		}
	}

	public function editPhoto( $id, PhotoEditForm $form):void
	{
		$photo = $this->photos->get($id);
		$photo->edit($form->album_id);
		$this->photos->save($photo);
	}
}