<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 21.04.2018
 * Time: 15:19
 */

namespace blog\entities;


use yii\db\ActiveRecord;

/**
 * Class Video
 * @package blog\entities
 *
 * @property integer $id
 * @property string $service
 * @property string $embed_code
 * @property string $url
 * @property string $large_thumbnail
 * @property string $name
 */
class Video extends ActiveRecord
{
	/**
	 * @param $service
	 * @param $embed_code
	 * @param $url
	 * @param $large_thumbnail
	 * @param $name
	 *
	 * @return Video
	 */
	public static function create($service, $embed_code, $url, $large_thumbnail, $name):self
	{
		$video = new Video();
		$video->name = $name;
		$video->service = $service;
		$video->embed_code = $embed_code;
		$video->url = $url;
		$video->large_thumbnail = $large_thumbnail;

		return $video;
	}

	/* @return string */
	public static function tableName()
	{
		return '{{%video}}';
	}


	/* @return array */
	public function attributeLabels()
	{
		return[
			'id' => 'ID',
			'video_id' => 'Video ID',
			'name' => 'Название',
			'service' => 'Видеохостинг',
			'embed_code' => 'Iframe',
			'url' => 'Ссылка',
			'large_thumbnail' => 'Картинка'
		];
	}

}