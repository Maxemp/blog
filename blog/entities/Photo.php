<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.03.2018
 * Time: 14:18
 */

namespace blog\entities;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * @property integer $id
 * @property string $file
 * @property integer $album_id
 *
 * @mixin ImageUploadBehavior
 */
class Photo extends ActiveRecord
{

	public static function create(UploadedFile $file, $album_id = null):self
	{
		$photo = new Photo();
		$photo->file = $file;
		$photo->album_id = $album_id;
		return $photo;
	}

	public function getAlbum():ActiveQuery
	{
		return $this->hasMany(Album::className(), ['id' => 'album_id']);
	}

	public function edit($album_id):void
	{
		$this->album_id = $album_id;
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'album_id' => 'Альбом',
			'file' => 'название фото'
		];
	}

	public static function tableName()
	{
		return '{{%photo}}';
	}

	public function behaviors()
	{
		return [
			[
				'class' => ImageUploadBehavior::className(),
				'attribute' => 'file',
				'createThumbsOnRequest' => true,
				'filePath' => '@staticRoot/origin/[[filename]]_[[pk]].[[extension]]',
				'fileUrl' => '@static/origin/[[filename]]_[[pk]].[[extension]]',
				'thumbPath' => '@staticRoot/cache/[[profile]]_[[filename]]_[[pk]].[[extension]]',
				'thumbUrl' => '@static/cache/[[profile]]_[[filename]]_[[pk]].[[extension]]',
				'thumbs' => [
					'admin' => ['width' => 100, 'height' => 70],
					'thumb' => ['width' => 640, 'height' => 480]
				],
			],
		];
	}

}