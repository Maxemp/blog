<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 18.06.2018
 * Time: 20:39
 */

namespace blog\entities;


use yii\db\ActiveRecord;
use blog\entities\User\User;

/**
 * Class Post
 * @package blog\entities
 *
 * @property integer $id
 * @property integer created_at
 * @property string $title
 * @property string $description
 * @property string $content
 * @property integer $created_by
 */
class Post extends ActiveRecord
{

	/**
	 * @param $title
	 * @param $description
	 * @param $content
	 * @param $created_by
	 *
	 * @return Post
	 */
	public static function create ($title, $description, $content, $created_by):Post
	{
		$post = new Post();
		$post->title = $title;
		$post->description = $description;
		$post->content = $content;
		$post->created_at = time();
		$post->created_by = $created_by;
		return $post;
	}

	public function getUser()
	{
		return $this->hasOne(User::class, ['id' => 'created_by']);
	}

	/**
	 * @param $title
	 * @param $description
	 * @param $content
	 */
	public function edit ($title, $description, $content):void
	{
		$this->title = $title;
		$this->description = $description;
		$this->content = $content;
	}

	public static function tableName()
	{
		return "{{%blog_post}}";
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'created_by' => 'Автор',
			'description' => 'Описание',
			'created_at' => 'Дата создания',
			'content' => 'Контент'
		];
	}

}