<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 30.04.2018
 * Time: 13:38
 */

namespace blog\entities;


use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * Class Document
 * @package blog\entities
 *
 * @property integer $id
 * @property string $document
 * @property string $name
 * @property string $number
 * @property string $date
 *
 * @mixin FileUploadBehavior
 */
class Document extends ActiveRecord
{
	/**
	 * @param UploadedFile $document
	 * @param $number
	 * @param $name
	 * @param $date
	 *
	 * @return Document
	 */
	public static function create(UploadedFile $document, $number, $name, $date):Document
	{
		$doc = new Document();
		$doc->document = $document;
		$doc->number = $number;
		$doc->name = $name;
		$doc->date = $date;

		return $doc;
	}

	/**
	 * @param $number
	 * @param $name
	 * @param $date
	 */
	public function edit($number, $name, $date):void
	{
		$this->number = $number;
		$this->name = $name;
		$this->date = $date;
	}

	public function behaviors()
	{

		return [
			[
				'class' => FileUploadBehavior::className(),
				'attribute' => 'document',
				'filePath' => '@staticRoot/documents/[[filename]]_[[pk]].[[extension]]',
				'fileUrl' => '@static/documents/[[filename]]_[[pk]].[[extension]]'
			]
		];

	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'number' => '№',
			'name' => 'Название Документа',
			'date' => 'Дата публикации'
		];
	}

	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{%document}}';
	}
}