<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 15.03.2018
 * Time: 22:05
 */

namespace blog\entities;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * @property integer $id
 * @property string $name
 */
class Album extends ActiveRecord
{

	public static function create($name): self
	{
		$album = new Album();
		$album->name = $name;
		return $album;
	}

	public function getPhoto(): ActiveQuery
	{
		return $this->hasMany(Photo::className(), ['album_id' => 'id']);
	}

	public static function tableName()
	{
		return '{{%album}}';
	}

	public function edit($name):void
	{
		$this->name = $name;
	}

}