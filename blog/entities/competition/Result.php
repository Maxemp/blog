<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 13.06.2018
 * Time: 22:54
 */

namespace blog\entities\competition;


use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * Class Result
 * @package blog\entities\competition
 *
 * @property integer $id
 * @property string $file
 * @property string $name
 * @property string $date_to
 * @property string $date_from
 * @property string $country
 * @property string $city
 *
 * @mixin FileUploadBehavior
 */
class Result extends ActiveRecord
{
	/**
	 * @param UploadedFile $file
	 * @param $name
	 * @param $date_from
	 * @param $date_to
	 * @param $country
	 * @param $city
	 *
	 * @return Result
	 */
	public static function create(UploadedFile $file, $name, $date_from, $date_to, $country, $city):Result
	{
		$result = new Result();
		$result->file = $file;
		$result->name = $name;
		$result->date_to = $date_to;
		$result->date_from = $date_from;
		$result->country = $country;
		$result->city = $city;

		return $result;
	}

	/**
	 * @param $name
	 * @param $date_to
	 * @param $date_from
	 * @param $country
	 * @param $city
	 */
	public function edit($name, $date_to, $date_from, $country, $city):void
	{
		$this->name = $name;
		$this->date_to = $date_to;
		$this->date_from = $date_from;
		$this->country = $country;
		$this->city = $city;
	}

	public function behaviors():array
	{

		return [
			[
				'class' => FileUploadBehavior::className(),
				'attribute' => 'file',
				'filePath' => '@staticRoot/results/[[filename]]_[[pk]].[[extension]]',
				'fileUrl' => '@static/results/[[filename]]_[[pk]].[[extension]]'
			]
		];

	}

	/**
	 * @return string
	 */
	public static function tableName():string
	{
		return "{{%result}}";
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'id' => 'ID',
			'file' => 'Файл',
			'name' => 'Название турнира',
			'date_from' => 'Начало турнира',
			'date_to' => 'Конец турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}
}