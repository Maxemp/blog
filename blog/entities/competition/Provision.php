<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 12.06.2018
 * Time: 16:18
 */

namespace blog\entities\competition;


use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * Class Provision
 * @package blog\entities\competition
 *
 * @property integer $id
 * @property string $date
 * @property string $name
 * @property string $file
 * @property string $url
 *
 * @mixin FileUploadBehavior
 */
class Provision extends ActiveRecord
{
	/**
	 * @param UploadedFile $file
	 * @param $date
	 * @param $name
	 * @param $url
	 *
	 * @return Provision
	 */
	public static function create(UploadedFile $file, $date, $name, $url):Provision
	{
		$provision = new Provision();
		$provision->date = $date;
		$provision->name = $name;
		$provision->file = $file;
		$provision->url = $url;

		return $provision;
	}

	/**
	 * @param $name
	 * @param $date
	 * @param $url
	 */
	public function edit($name, $date, $url):void
	{
		$this->date = $date;
		$this->name = $name;
		$this->url = $url;
	}

	public function behaviors()
	{

		return [
			[
				'class' => FileUploadBehavior::className(),
				'attribute' => 'file',
				'filePath' => '@staticRoot/provisions/[[filename]]_[[pk]].[[extension]]',
				'fileUrl' => '@static/provisions/[[filename]]_[[pk]].[[extension]]'
			]
		];

	}

	/**
	 * @return string
	 */
	public static function tableName()
	{
		return "{{%provision}}";
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'date' => 'Дата публикации',
			'name' => 'Название документа',
			'url' => 'Ссылка на скачивание',
		];
	}

}