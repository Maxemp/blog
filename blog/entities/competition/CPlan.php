<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 02.05.2018
 * Time: 14:35
 */

namespace blog\entities\competition;


use yii\db\ActiveRecord;

/**
 * CPlan - Calendar Plan
 * Class CPlan
 * @package blog\entities\competition
 *
 * @property integer $id
 * @property string $date
 * @property string $age_group
 * @property string $name
 * @property string $country
 * @property string $city
 */
class CPlan extends ActiveRecord
{
	/**
	 * @return string
	 */
	public static function tableName():string
	{
		return "{{%calendar_plan}}";
	}

	/**
	 * @param $date
	 * @param $age_group
	 * @param $name
	 * @param $country
	 * @param $city
	 *
	 * @return CPlan
	 */
	public static function create($date, $age_group, $name, $country, $city):CPlan
	{
		$calendar_plan = new CPlan();
		$calendar_plan->date = $date;
		$calendar_plan->age_group = $age_group;
		$calendar_plan->name = $name;
		$calendar_plan->country = $country;
		$calendar_plan->city = $city;

		return $calendar_plan;
	}

	/**
	 * @param $date
	 * @param $age_group
	 * @param $name
	 * @param $country
	 * @param $city
	 */
	public function edit($date, $age_group, $name, $country, $city):void
	{
		$this->date = $date;
		$this->age_group = $age_group;
		$this->name = $name;
		$this->country = $country;
		$this->city = $city;
	}

	/**
	 * @return array
	 */
	public function attributeLabels():array
	{
		return [
			'id' => 'ID',
			'date' => 'Даты проведения',
			'age_group' => 'Возрастная группа',
			'name' => 'Название турнира',
			'country' => 'Страна',
			'city' => 'Город',
		];
	}

}