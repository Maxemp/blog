<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 09.02.2018
 * Time: 2:08
 */

namespace blog\services\auth;


use blog\entities\User\User;
use blog\forms\auth\SignupForm;
use blog\repositories\UserRepository;
use yii\mail\MailerInterface;

class SignUpService
{
	private $users;
	private $mailer;

	public function __construct(MailerInterface $mailer,UserRepository $users)
	{
		$this->mailer = $mailer;
		$this->users =$users;
	}

	public function signup(SignupForm $form): void
	{
		$user = User::requestSignup(
			$form->username,
			$form->email,
			$form->password
		);

		$this->users->save($user);
		$sent = $this->mailer->compose(
			['html' => 'emailConfirmToken-html', 'text' => 'emailConfirmToken-text'],
			['user' => $user]
		)
		->SetTo($form->email)
		->setSubject('SignUp confirm for'. \Yii::$app->name)
		->send();

		if (!$sent){
			throw new \DomainException('Email sending error');
		}
	}

	public function confirm($token):void
	{
		if(empty($token)){
			throw new \DomainException('Empty confirm token');
		}
		$user = $this->users->getByEmailConfirmToken($token);
		$user->confirmSignup();
		$this->users->save($user);
	}
}