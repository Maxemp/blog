<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 09.02.2018
 * Time: 1:48
 */

namespace blog\services\auth;


use blog\entities\User\User;
use blog\forms\auth\LoginForm;
use blog\repositories\UserRepository;

class AuthService
{

	private $users;

	public function __construct(UserRepository $users)
	{
		$this->users =$users;
	}

	public function auth(LoginForm $form):User
	{
		$user = $this->users->findByUsernameOrEmail($form->username);
		if(!$user || !$user->isActive() || !$user->validatePassword($form->password)){
			throw new \DomainException('Undefined user or password.');
		}
		return $user;
	}
}