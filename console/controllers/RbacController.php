<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 27.06.2018
 * Time: 20:26
 */

namespace console\controllers;


use yii\console\Controller;

class RbacController extends Controller
{

	public function actionInit()
	{
		$auth = \Yii::$app->authManager;

		$auth->removeAll();

		$createPost = $auth->createPermission('createPost');
		$createPost->description = 'Создание статей';
		$auth->add($createPost);

		$updatePost = $auth->createPermission('updatePost');
		$updatePost->description = 'Редактирование статей';
		$auth->add($updatePost);

		$viewAdminPage = $auth->createPermission('viewAdminPage');
		$viewAdminPage->description = 'Возможность заходить в админку';
		$auth->add($viewAdminPage);

		$user = $auth->createRole('user');
		$auth->add($user);
		$auth->addChild($user, $createPost);

		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$auth->addChild($admin, $updatePost);
		$auth->addChild($admin, $viewAdminPage);
		$auth->addChild($admin, $user);

		$auth->assign($admin, 1);
		$auth->assign($user, 3);
	}

}