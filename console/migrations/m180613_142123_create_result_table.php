<?php

use yii\db\Migration;

/**
 * Handles the creation of table `result`.
 */
class m180613_142123_create_result_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%result}}', [
            'id' => $this->primaryKey(),
	        'file' => $this->string()->notNull(),
	        'name' => $this->string()->notNull(),
	        'date_to' => $this->string(),
	        'date_from' => $this->string(),
	        'country' => $this->string()->notNull(),
	        'city' => $this->string()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%result}}');
    }
}
