<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_post`.
 */
class m180618_115343_create_blog_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    $this->createTable('{{%blog_post}}', [
            'id' => $this->primaryKey(),
	        'title' => $this->string()->notNull(),
		    'created_at' => $this->integer()->notNull(),
		    'description' => $this->string()->notNull(),
		    'content' => 'MEDIUMTEXT',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_post}}');
    }
}
