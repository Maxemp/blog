<?php

use yii\db\Migration;

/**
 * Handles the creation of table `document`.
 */
class m180430_061410_create_document_table extends Migration
{
	/**
     * @inheritdoc
     */
    public function SafeUp()
    {
    	$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%document}}', [
            'id' => $this->primaryKey(),
	        'name' => $this->string()->notNull(),
	        'document' => $this->string()->notNull(),
	        'number' => $this->string()->notNull(),
	        'date' => $this->integer()->notNull()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function SafeDown()
    {
        $this->dropTable('{{%document}}');
    }
}
