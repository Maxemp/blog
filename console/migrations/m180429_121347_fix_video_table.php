<?php

use yii\db\Migration;

/**
 * Class m180429_121347_fix_video_table
 */
class m180429_121347_fix_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->addColumn('{{%video}}', 'name', $this->string()->notNull());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
		$this->dropColumn('{{%video}}', 'name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_121347_fix_video_table cannot be reverted.\n";

        return false;
    }
    */
}
