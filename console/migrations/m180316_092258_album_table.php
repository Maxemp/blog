<?php

use yii\db\Migration;

/**
 * Class m180316_092258_album_table
 */
class m180316_092258_album_table extends Migration
{

    public function safeUp()
    {
		$tableOptions = "CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB";
		$this->createTable( '{{%album}}' ,[
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull()
		], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%album}}');
    }

}
