<?php

use yii\db\Migration;

/**
 * Handles the creation of table `calendar_plan`.
 */
class m180502_064531_create_calendar_plan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
    	$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%calendar_plan}}', [
            'id' => $this->primaryKey(),
	        'date' => $this->string()->notNull(),
	        'age_group' => $this->string()->notNull(),
	        'name' => $this->string()->notNull(),
	        'country' => $this->string()->notNull(),
	        'city' => $this->string()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%calendar_plan}}');
    }
}
