<?php

use yii\db\Migration;

/**
 * Handles the creation of table `provision`.
 */
class m180612_075025_create_provision_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%provision}}', [
            'id' => $this->primaryKey(),
	        'date' => $this->string()->notNull(),
	        'name' => $this->string()->notNull(),
	        'file' => $this->string(),
	        'url' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%provision}}');
    }
}
