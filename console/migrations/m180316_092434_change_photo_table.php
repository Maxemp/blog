<?php

use yii\db\Migration;

/**
 * Class m180316_092434_change_photo_table
 */
class m180316_092434_change_photo_table extends Migration
{


    public function safeUp()
    {
		$this->addColumn('{{%photo}}', 'album_id', $this->integer());

		$this->addForeignKey(
			'fk-photo-album_id',
			'{{%photo}}',
			'album_id',
			'{{%album}}',
			'id',
			'CASCADE'
		);

		$this->createIndex(
			'idx-photo-album_id',
			'{{%photo}}',
			'album_id'
		);
    }

    public function safeDown()
    {
    	$this->dropColumn('{{%photo}}', 'album_id');

    	$this->dropForeignKey('fk-photo-album_id', '{{%photo}}');

    	$this->dropIndex('idx-photo-album_id', '{{%photo}}');
    }

}
