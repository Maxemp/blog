<?php

use yii\db\Migration;

/**
 * Class m180627_115903_create_create_by
 */
class m180627_115903_create_create_by extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->addColumn("{{%blog_post}}", 'created_by', $this->integer()->notNull());
	    $this->addForeignKey('{{%fk-blog_post-user}}', '{{%blog_post}}','created_by', '{{%user}}', 'id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    	$this->dropColumn("{{%blog_post}}", 'created_by');
    	$this->dropForeignKey("{{%fk-blog_post-user}}}", "{{%blog_post}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_115903_create_create_by cannot be reverted.\n";

        return false;
    }
    */
}
