<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo`.
 */
class m180305_081859_create_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
	        'file' => $this->string()->notNull(),

        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%photo}}');
    }
}
