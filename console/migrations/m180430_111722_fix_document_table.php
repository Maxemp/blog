<?php

use yii\db\Migration;

/**
 * Class m180430_111722_fix_document_table
 */
class m180430_111722_fix_document_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->alterColumn('{{%document}}', 'date', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%document}}', 'date', $this->integer()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180430_111722_fix_document_table cannot be reverted.\n";

        return false;
    }
    */
}
