<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video`.
 */
class m180428_071632_create_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
    	$tableOptions = "CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB";
        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(),
	        'service' => $this->string(),
	        'url' => $this->string(),
	        'embed_code' => $this->text(),
	        'large_thumbnail' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%video}}');
    }
}
