<?php
return [
	'cookieValidationKey' => '',
	'cookieDomain' => '.blog.loc',
	'frontendHostInfo' => 'http://blog.loc',
	'backendHostInfo' => 'http://backend.blog.loc',
	'staticHostInfo' => 'http://static.blog.loc',
];
