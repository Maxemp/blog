<?php
return [
	'cookieValidationKey' => '',
	'cookieDomain' => '.my1blog.tech',
	'frontendHostInfo' => 'http://my1blog.tech',
	'backendHostInfo' => 'http://backend.my1blog.tech',
	'staticHostInfo' => 'http://static.my1blog.tech',
];