<?php

namespace common\bootstrap;
use blog\services\contact\ContactService;
use yii\base\BootstrapInterface;
use blog\services\auth\PasswordResetService;
use yii\mail\MailerInterface;
use yii\rbac\ManagerInterface;

class SetUp implements BootstrapInterface
{
	public function bootstrap( $app ):void
	{
		$container = \Yii::$container;

		$container->setSingleton(PasswordResetService::class);

		$container->setSingleton(MailerInterface::class, function () use($app)
		{
			return $app->mailer;
		});

		$container->setSingleton(ContactService::class, [], [
			$app->params['adminEmail']
		]);

		$container->setSingleton(ManagerInterface::class, function () use ($app) {
			return $app->authManager;
		});
	}
}
