<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'user.rememberMeDuration' => 3600 * 24 * 30,
	'cookieDomain' => '.example.com',
	'frontendHostInfo' => 'http://blog.loc',
	'backendHostInfo' => 'http://backend.blog.loc',
	'staticHostInfo' => 'http://static.blog.loc',
	'staticPath' => dirname(__DIR__, 2).'/static',
];
